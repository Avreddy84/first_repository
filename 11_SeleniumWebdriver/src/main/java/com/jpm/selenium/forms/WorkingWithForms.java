package com.jpm.selenium.forms;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.jpm.selenium.util.ChromeUtil;

public class WorkingWithForms {

	public static void main(String[] args) throws InterruptedException {
		//load driver and open browser
		WebDriver driver = ChromeUtil.getChromeDriver();
		//open the Webpage
		
		driver.get("file:\\D:\\Pradeep\\Day51\\11_SeleniumWebdriver\\src\\main\\java\\WorkingWithForms.html");
		//maximize the browser window
		driver.manage().window().maximize();
		driver.findElement(By.id("txtUserName")).sendKeys("prad");
		driver.findElement(By.id("txtPassword")).sendKeys("password");//password
		driver.findElement(By.className("Format")).sendKeys("password");//confirm password
		driver.findElement(By.cssSelector("Input.Format1")).sendKeys("pradeep");
		driver.findElement(By.cssSelector("Input#txtLastName")).sendKeys("reddy");
	
		List<WebElement> radioElements=driver.findElements(By.name("gender"));
		for(WebElement radio:radioElements ){
			String radioSelection = radio.getAttribute("value").toString();
			//which radio button to be selected
			if(radioSelection.equals("Male")){
				radio.click();
				
			}
		}
		
		driver.findElement(By.cssSelector("input[type=date]")).sendKeys("12/12/2001");
		driver.findElement(By.id("txtEmail")).sendKeys("prad@gmail.com");
		//dropdown
		driver.findElement(By.name("Address")).sendKeys("Hyderabad");
		Select drpCity= new Select(driver.findElement(By.name("City")));
		//Dropdown 3 ways ->selectByIndex, selectByvalue,selectByVisibleText
		drpCity.selectByIndex(3);
		drpCity.selectByValue("Mumbai");
		drpCity.selectByVisibleText("Pune");
		
		driver.findElement(By.name("Phone")).sendKeys("52352352323");
		List<WebElement> checkEleList=driver.findElements(By.name("chkHobbies"));
		for(WebElement hobbyChkBox:checkEleList){
			String selection = hobbyChkBox.getAttribute("value").toString();
			if(!selection.equals("Movies")){//not click Movies
				hobbyChkBox.click();
			}
		}
		Thread.sleep(10000);
		driver.close();
		System.out.println("Registration on process!");
	}

}
