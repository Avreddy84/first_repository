package com.jpm.selenium.popup;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.jpm.selenium.util.ChromeUtil;

public class PopUpWindowDemo {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver = ChromeUtil.getChromeDriver();
		String url="file:\\D:\\Pradeep\\Day51\\11_SeleniumWebdriver\\src\\main\\java\\PopUpWinDemo.html";
		driver.get(url);
		String parentWin = driver.getWindowHandle();
		Thread.sleep(2000);
		driver.findElement(By.id("newtab")).click();
		parentWin = driver.getWindowHandle();//parent window
		System.out.println("Parent Win:"+parentWin);
		Thread.sleep(2000);
		Set<String> childWinList=driver.getWindowHandles();//all the window used
		for(String childWin:childWinList){
			if(!childWin.equals(parentWin)){
				Thread.sleep(2000);
				driver.switchTo().window(childWin);
				System.out.println("Child Window:"+childWin);
				driver.findElement(By.id("alert")).click();
				Thread.sleep(2000);
				Alert alert = driver.switchTo().alert();
				Thread.sleep(2000);
				alert.accept();//use dismiss() method to cancel
			}
		}
		driver.switchTo().window(parentWin);
		System.out.println("switch to parent win :"+parentWin);
		Thread.sleep(2000);
		System.out.println("Again click to another child window :"+parentWin);
		driver.findElement(By.id("newwindow")).click();
		for(String childWin2:driver.getWindowHandles()){
			
			if(!childWin2.equals(parentWin)){
				driver.switchTo().window(childWin2);
				System.out.println("Child window2:"+childWin2);
				Thread.sleep(1000);
			
			}
		}
		Thread.sleep(1000);
		driver.quit();
		

	}

}
