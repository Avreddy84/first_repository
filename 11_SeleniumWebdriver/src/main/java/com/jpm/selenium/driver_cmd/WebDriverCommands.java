package com.jpm.selenium.driver_cmd;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.jpm.selenium.util.ChromeUtil;

public class WebDriverCommands {

	public static void main(String[] args) throws InterruptedException {
		//we need instance of driver
		WebDriver driver = ChromeUtil.getChromeDriver();
		if(driver!=null){
			//get->to launch the website
			//String url="https://www.synergetics-india.com/";
			 String url="file:\\D:\\Pradeep\\Day51\\11_SeleniumWebdriver\\src\\main\\java\\Locators.html";
			driver.get(url);
			System.out.println("Title of the page is :"+driver.getTitle());
			System.out.println("Current Url:"+driver.getCurrentUrl());
			//By.id
			WebElement element=driver.findElement(By.id("user"));
			System.out.println(" element 1 :"+element.getAttribute("id"));
			element.sendKeys("admin");//input the data in that element
			driver.findElement(By.name("password")).sendKeys("password@123");
			element = driver.findElement(By.linkText("How to use locators?"));
			System.out.println("element link text :"+element.getText());
			Thread.sleep(2000);
			element.click();//it will click the link as the WebElement is holding link reference
			
		}else{
			System.out.println("Driver not loaded!");
		}
		Thread.sleep(5000);
		//driver.close();
	}

}
