package com.jpm.selenium.table;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.FindElement;

import com.jpm.selenium.util.ChromeUtil;

public class TabelDemo {

	public static void main(String[] args) {
		//Load chrome driver and open browser
		
		WebDriver driver = ChromeUtil.getChromeDriver();
		String url="file:\\D:\\first_repository\\11_SeleniumWebdriver\\src\\main\\java\\table.html";
		driver.get(url);
		//Implicit wait- tell to webdriver to wait for certain amount of time before it throws a "No such Element Exception"
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		//locating table
		WebElement mytable = driver.findElement(By.tagName("tbody"));
		//locating rows
		List<WebElement> rows_table = mytable.findElements(By.tagName("tr"));
		
		//no of row
		int row_count = rows_table.size();
		System.out.println("No of rows"+row_count);
		for(int row=0;row<row_count;row++){
			
			List<WebElement> cols_table = rows_table.get(row).findElements(By.tagName("td"));
			int cols_count = cols_table.size();
			System.out.println("Row No: "+row + "has "+ cols_count+"columns");
			//print the table data
			for(int col=0;col<cols_count;col++){
				String tableData = cols_table.get(col).getText();
				System.out.println("Cell Value :"+tableData);
			}
			
		}
		
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Implicit wait time is applied to all the elements in the script
		//we cant specify "ExceptionConditions" on the element to be located
		//implicit wait is used when the elements are located with the same time frame
		
		//Explicit wait - it is used to tell the webDriver to wait for certain condition
		//(ExpectedConditions) or the maximum time execeed before throwing an "ElementNotVisibleException"
		//it can be applied only for specified element
		//example category is dynamically loaded from the DB in the dropdown through Ajax call better to use explicit wait
		
		driver.close();
		
	}

}
